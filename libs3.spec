%global commitdate 20150611
%global commit 11a4e976c28ba525e7d61fbc3867c345a2af1519
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name: libs3
Version: 2.0
Release: 0.1.%{commitdate}git%{shortcommit}%{?dist}
Summary: C Library and Tools for Amazon S3 Access
License: LGPLv3
URL: https://github.com/bji/libs3
Group: System Environment/Libraries
Source0:  https://github.com/bji/%{name}/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz

BuildRequires:  curl-devel
BuildRequires:  libxml2-devel

%description
This package includes the libs3 shared object library, needed to run
applications compiled against libs3, and additionally contains the s3
utility for accessing Amazon S3.

%package devel
Summary: Headers and documentation for libs3
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This library provides an API for using Amazon's S3 service (see
http://s3.amazonaws.com).  Its design goals are:

 - To provide a simple and straightforward API for accessing all of S3's
   functionality
 - To not require the developer using libs3 to need to know anything about:
     - HTTP
     - XML
     - SSL
   In other words, this API is meant to stand on its own, without requiring
   any implicit knowledge of how S3 services are accessed using HTTP
   protocols.
 - To be usable from multithreaded code
 - To be usable by code which wants to process multiple S3 requests
   simultaneously from a single thread
 - To be usable in the simple, straightforward way using sequentialized
   blocking requests

%prep
%setup -q -n %{name}-%{commit}

%build
make %{?_smp_mflags} exported

%check
# Does this work with libs3?
#make check

%install
DESTDIR=%{buildroot}%{_prefix} LIBDIR=%{buildroot}%{_libdir} make install

# Remove static libtool archive:
rm %{buildroot}%{_libdir}/libs3.a

%post
%if 0%{?el6}
  /sbin/ldconfig
%else
  %{_sbindir}/ldconfig
%endif

%postun
%if 0%{?el6}
  /sbin/ldconfig
%else
  %{_sbindir}/ldconfig
%endif

%files
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_bindir}/s3
%{_libdir}/libs3.so.*

%files devel
%{_includedir}/libs3.h
%{_libdir}/libs3.so

%changelog
* Fri Jun 12 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.0-0.1.20150611git11a4e97
- Initial package
